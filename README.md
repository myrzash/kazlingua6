# ҚазLingua 6 класс.

## AEO NIS Centre for Educational Programmes

Приложение поможет изучить казахский язык быстрее.
База заданий основана на частотном словаре 6 класса.

![ҚазLingua](/assets/menu_header.png)

## Приоритеты ҚазLingua:

- задания даются в игровой форме;
- на основе часто используемых слов;
- каждый урок занимает 5-10 минут;
- способность контролировать результат;

В конце каждой темы студент может получить информацию о своих достижениях.
Часто используемые слова представлены в интерактивном режиме.

### Состоит из:

- 13 разделов
- каждый раздел по 4-6 уроков.
- каждый урок по 6 видов интерактива.
- есть функция "Поделиться с другом"
- хранение результатов локально

### AppStore

- Иконка для appstore в директории assets/
- Проект под iOS в директории ios/
- запускать с помощью Xcode

### PlayMarket

- Иконка для playmarket в директории assets/
- Проект под Android в директории android/
- запускать с помощью AndroidStudio
