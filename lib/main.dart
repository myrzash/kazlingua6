import 'package:flutter/material.dart';
import 'package:kazlingua6/pages/MainPage.dart';
import 'package:kazlingua6/pages/AboutPage.dart';
import 'package:kazlingua6/pages/GamePage.dart';
import 'package:kazlingua6/pages/PartPage.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:kazlingua6/models/MainModel.dart';

void main() => runApp(
      new ScopedModel<MainModel>(
        model: MainModel(),
        child: MaterialApp(
          initialRoute: '/',
          debugShowCheckedModeBanner: false,
          routes: {
            '/': (context) => MainPage(),
            '/about': (context) => AboutPage(),
            '/part': (context) => PartPage(),
            '/game': (context) => GamePage(),
          },
        ),
      ),
    );
