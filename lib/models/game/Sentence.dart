class Sentence {
  String ru;
  String kz;

  Sentence({this.ru, this.kz});

  Sentence.fromJSON(json) {
    kz = json["kz"].trim().replaceAll(new RegExp(r"\s+"), " ");
    ru = json["ru"];
  }

}
