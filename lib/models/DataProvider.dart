import 'package:flutter/material.dart';
import 'package:kazlingua6/models/Part.dart';
import 'package:kazlingua6/datas/colors.dart';
import 'dart:convert';

class DataProvider {
  Future<List<Part>> fetchParts(context) async {
    String path = "assets/data.json";
    String json = await DefaultAssetBundle.of(context).loadString(path);
    List list = jsonDecode(json);
    List<Part> parts = list.map((json) => Part.fromJSON(json)).toList();

    parts.asMap().forEach((i, part) {
      part.bgColor = colors[i % colors.length];
    });

    return parts;
  }
}
