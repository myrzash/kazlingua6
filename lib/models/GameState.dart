import 'package:flutter/material.dart';

class GameState extends InheritedWidget {
  final Function onNext;
  final Function onCorrect;

  const GameState({
    Key key,
    @required this.onNext,
    @required this.onCorrect,
    @required Widget child,
  })  : assert(child != null),
        super(key: key, child: child);

  static GameState of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<GameState>();
  }

  @override
  bool updateShouldNotify(GameState old) => true;
}
