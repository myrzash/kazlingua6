import 'package:kazlingua6/models/game/Word.dart';
import 'package:kazlingua6/models/game/Test.dart';
import 'package:kazlingua6/models/game/Sentence.dart';
import 'package:kazlingua6/models/game/Sort.dart';
import 'package:kazlingua6/models/game/Skip.dart';
import 'package:kazlingua6/models/game/Listen.dart';
import 'package:kazlingua6/helpers/Prefs.dart';

class Unit {
  String keyScore = '';

  int score = 0;
  String title;
  List<Word> words;
  List<Test> test;
  List<Sentence> sentences;
  List<Sort> sorts;
  List skip;
  List<Listen> listens;

//
//  Unit({
//    this.title,
//    this.words,
//    this.test,
//    this.sentences,
//    this.sorts,
//    this.skip,
//    this.listens,
//  });

  Unit.fromJSON(json, String partName) {
    title = json["title"];

    List words = json["words"];
    this.words =
        words == null ? [] : words.map((json) => Word.fromJSON(json)).toList();

    List test = json["test"];
    this.test =
        test == null ? [] : test.map((json) => Test.fromJSON(json)).toList();

    List sentences = json["sentences"];
    this.sentences = sentences == null
        ? []
        : sentences.map((json) => Sentence.fromJSON(json)).toList();

    List sorts = json["sort"];
    this.sorts =
        sorts == null ? [] : sorts.map((json) => Sort.fromJSON(json)).toList();

    List skip = json["skip"];
    this.skip =
        skip == null ? [] : skip.map((json) => Skip.fromJSON(json)).toList();

    List listens = json["listens"];
    this.listens = listens == null
        ? []
        : listens.map((json) => Listen.fromJSON(json)).toList();

    keyScore = '$partName&$title';
    upgradeScore();
  }

  bool get isComplete => score > 0;

  bool get isEmpty {
    if (words.isNotEmpty) return false;

    if (test.isNotEmpty) return false;

    if (sentences.isNotEmpty) return false;

    if (sorts.isNotEmpty) return false;

    if (skip.isNotEmpty) return false;

    if (listens.isNotEmpty) return false;

    return true;
  }

  void upgradeScore() async {
    int val = await Prefs.shared.getResult(keyScore);
    if (val != null && val is int && val > score && val <= 100) {
      score = val;
    }
  }
}
