import 'package:flutter/material.dart';
import 'package:kazlingua6/models/Unit.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:kazlingua6/models/MainModel.dart';

class UnitCard extends StatefulWidget {
  final int pageNumber;
  final int countPage;
  final Unit unit;

  UnitCard({@required this.unit, this.pageNumber, this.countPage});

  @override
  _UnitCardState createState() => _UnitCardState();
}

class _UnitCardState extends State<UnitCard> {
  final double fontSize = 18;

  String getWordsAsString() {
    List<String> words = []
      ..addAll(widget.unit.words.map((it) => it.kz))
      ..addAll(widget.unit.listens.map((it) => it.kz));
    words = words.toSet().toList();
    if (words.length > 6) words = words.getRange(0, 6).toList();
    return words.join(", ").toLowerCase();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Opacity(
        opacity: widget.unit.isEmpty ? 0.8 : 1,
        child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24.0),
            side: widget.unit.isComplete
                ? new BorderSide(color: Colors.amber, width: 6)
                : new BorderSide(color: Colors.transparent, width: 0),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text('${widget.unit.title}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: fontSize,
                          fontWeight: FontWeight.w600,
                        )),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('${widget.pageNumber} из ${widget.countPage}',
                            style: TextStyle(
                              color: widget.unit.isComplete
                                  ? Colors.amber
                                  : Colors.grey[500],
                              fontSize: fontSize,
                              fontWeight: FontWeight.w500,
                            )),
                        if (widget.unit.isComplete) SizedBox(width: 10),
                        if (widget.unit.isComplete)
                          Icon(
                            Icons.check_circle,
                            color: Colors.amber,
                          )
                      ],
                    ),
                  ],
                ),
                Center(
                  child: Text(
                    getWordsAsString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      letterSpacing: .7,
                      fontSize: fontSize * 1.1,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                ScopedModelDescendant<MainModel>(
                  rebuildOnChange: true,
                  builder: (context, _, model) => RaisedButton(
                    color:
                        widget.unit.isComplete ? Colors.amber : Colors.blueGrey,
                    onPressed: widget.unit.isEmpty
                        ? null
                        : () {
                            model.selectUnit(widget.unit);
                            Navigator.pushNamed(context, '/game');
                          },
                    padding: EdgeInsets.all(10),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Text(
                      widget.unit.isComplete ? 'ПОВТОРИТЬ' : 'НАЧАТЬ',
                      style: TextStyle(
                        letterSpacing: 1,
                        color: Colors.white,
                        fontSize: fontSize,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
