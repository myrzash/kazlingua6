import 'package:flutter/material.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:kazlingua6/views/UnitCard.dart';
import 'package:kazlingua6/models/Unit.dart';

class UnitsPageSlider extends StatefulWidget {
  final Color colorIndicatior;
  final List<Unit> units;

  UnitsPageSlider({this.colorIndicatior = Colors.grey, this.units});

  @override
  _UnitsPageSliderState createState() {
    List<Widget> children = getUnitCards(units);

    return _UnitsPageSliderState(
        children: children, colorIndicatior: colorIndicatior);
  }

  List<Widget> getUnitCards(List<Unit> list) {
    int pageCount = list.length;

    return list
        .asMap()
        .map((index, unit) => MapEntry(
              index,
              UnitCard(unit: unit, pageNumber: index + 1, countPage: pageCount),
            ))
        .values
        .toList();
  }
}

class _UnitsPageSliderState extends State<UnitsPageSlider> {
  final List<Widget> children;
  int currentPage = 0;
  final controller = PageController(initialPage: 0);
  final Color colorIndicatior;

  _UnitsPageSliderState({this.children, this.colorIndicatior});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Flexible(
          child: PageView(
            physics: BouncingScrollPhysics(),
            onPageChanged: (int page) {
              setState(() {
                currentPage = page;
              });
            },
            controller: controller,
            children: children,
          ),
        ),
        Flexible(
          flex: 0,
          child: DotsIndicator(
            dotsCount: children.length,
            position: double.parse("$currentPage"),
            decorator: DotsDecorator(
              spacing: const EdgeInsets.symmetric(horizontal: 5, vertical: 15),
              activeColor: colorIndicatior,
            ),
          ),
        )
      ],
    );
  }
}
