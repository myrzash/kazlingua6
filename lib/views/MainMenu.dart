import 'package:flutter/material.dart';
import 'package:share/share.dart';

class MainMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 240,
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            SizedBox(
              height: 260,
              child: DrawerHeader(
                margin: EdgeInsets.zero,
                padding: EdgeInsets.zero,
                child: Image.asset(
                  'assets/menu_header.png',
                  fit: BoxFit.fitWidth,
                ),
                decoration: BoxDecoration(
                  color: const Color(0xFF009ed6),
                ),
              ),
            ),
            ListTile(
                leading: Icon(Icons.share),
                title: Text('Поделиться'),
                onTap: () => shareApp(context)),
            ListTile(
                leading: Icon(Icons.info_outline),
                title: Text('О приложении'),
                onTap: () => goToAboutPage(context)),
          ],
        ),
      ),
    );
  }

  void goToAboutPage(context) {
    Navigator.pop(context);
    Navigator.pushNamed(context, '/about');
  }

  void shareApp(context) {
    Navigator.pop(context);
    Share.share('Қазlingua 6 класс.\n\n'
        'Web-версия:\n https://der.nis.edu.kz/kazlingua/grade5\n\n'
        'PlayMarket:\n https://play.google.com/store/apps/details?id=kz.nis.kazlingua6\n\n'
        'AppStore:\n https://apps.apple.com/us/app/қазlingua-5-класс/id1494852817');
  }
}
