import 'package:flutter/material.dart';
import 'package:kazlingua6/models/Part.dart';
import 'package:kazlingua6/views/PartCircle.dart';
import 'package:transparent_image/transparent_image.dart';

class PartCard extends StatelessWidget {
  final Part part;
  final Function onItemClick;

  PartCard({this.part, this.onItemClick});

  @override
  Widget build(BuildContext context) {
    bool isNotComplete = part.units.any((item) => !item.isComplete);

    return Card(
      elevation: isNotComplete ? 1 : 0,
      color: part.bgColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: InkWell(
        onTap: onItemClick,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Hero(
                tag: '${part.image}',
                child: CircleAvatar(
                  radius: 60,
                  backgroundColor: Colors.transparent,
                  child: FadeInImage(
                    placeholder: MemoryImage(kTransparentImage),
                    image: AssetImage('assets/part_icons/${part.image}'),
                  ),
                ),
              ),
              SizedBox(height: 4),
              Text(
                part.name,
                textAlign: TextAlign.center,
                maxLines: 3,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  height: 1.2,
                ),
              ),
              SizedBox(height: 2),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: part.units
                    .map((unit) => Container(
                          padding: EdgeInsets.symmetric(horizontal: 2),
                          child: PartCircle(isFull: unit.isComplete),
                        ))
                    .toList(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
