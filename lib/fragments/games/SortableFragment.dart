import 'package:flutter/material.dart';
import 'package:kazlingua6/fragments/BaseGameFragment.dart';

class SortableFragment extends GameFragment {
  final String audio;
  final List<String> sentences;

  SortableFragment({Key key, this.audio, this.sentences}) : super(key: key);

  @override
  _SortableFragmentState createState() => _SortableFragmentState();
}

class _SortableFragmentState extends GameFragmentState<SortableFragment>
    with BasicGame {
  List<String> list;
  bool isDragged = false;
  bool localActivateNext = true;

  @override
  void initState() {
    super.initState();
    list = []
      ..addAll(widget.sentences)
      ..shuffle();
  }

  @override
  bool get activeCheck => isDragged;

  @override
  bool get activeNext => localActivateNext;

  @override
  String get task => 'Сөйлемдерді ретімен орналастыр';

  @override
  bool get isCorrect => list.join(';') == widget.sentences.join(';');

  @override
  Widget body() {
    return Column(
      children: <Widget>[
        SizedBox(height: 20),
        Expanded(
          flex: 1,
          child: ReorderableListView(
            children: list
                .asMap()
                .map((index, item) => MapEntry(
                      index,
                      Card(
                        key: ValueKey(item),
                        elevation: 0,
                        child: ListTile(
                          title: Text(
                            '$item',
                            style: TextStyle(
                              fontSize: 18,
                              color: getColorText(item, index),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
//                          trailing: getIcon(item, index),
                        ),
                      ),
                    ))
                .values
                .toList(),
            onReorder: (oldIndex, newIndex) {
              newIndex = newIndex < 0 ? 0 : newIndex;
              newIndex = newIndex >= list.length ? list.length - 1 : newIndex;

              setState(() {
                if (!isDragged) isDragged = true;
                String buf = list[oldIndex];
                list[oldIndex] = list[newIndex];
                list[newIndex] = buf;
              });
            },
          ),
        )
      ],
    );
  }

  @override
  void onWrong() async {
    setState(() {
      this.localActivateNext = false;
    });
    await Future.delayed(Duration(milliseconds: 1500), () {
      setState(() {
        this.localActivateNext = true;
      });
    });
  }

  Color getColorText(item, index) {
    if (!checked) return Colors.black;

    bool isCorrect = widget.sentences[index] == item;
    return isCorrect ? Colors.green[400] : Colors.red[400];
  }

//  Icon getIcon(item, index) {
//    if (!checked) return Icon(Icons.menu);
//
//    bool isCorrect = widget.sentences[index] == item;
//    return isCorrect
//        ? Icon(
//            Icons.check,
//            color: Colors.green[400],
//          )
//        : Icon(
//            Icons.close,
//            color: Colors.red[400],
//          );
//  }
}
