import 'package:flutter/material.dart';
import 'package:kazlingua6/fragments/BaseGameFragment.dart';
import 'package:kazlingua6/models/game/Skip.dart';

class SkipFragment extends GameFragment {
  final List list;

  SkipFragment({Key key, this.list}) : super(key: key);

  @override
  _SkipFragmentState createState() => _SkipFragmentState();
}

class _SkipFragmentState extends GameFragmentState<SkipFragment>
    with BasicGame {
  List list = [];
  List<String> skips = [];
  List<String> correctAnswers = [];
  List<String> trash = [];

  bool localActivateNext = true;

  @override
  void initState() {
    super.initState();
    list.clear();
    trash.clear();
    correctAnswers.clear();

    widget.list.forEach((item) {
      if (item is SkipItem) {
        list.add(null);
        correctAnswers.add(item.skip);
      } else
        list.addAll(item.toString().split(' '));
    });

    skips = []
      ..addAll(correctAnswers)
      ..shuffle();
  }

  @override
  String get task => 'Қажет сөзді таңда';

  @override
  bool get activeCheck => skips.length == 0;

  @override
  bool get activeNext => localActivateNext;

  @override
  Widget body() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 10),
            Expanded(
              flex: 0,
              child: Card(
                elevation: 0,
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Wrap(
                    spacing: 5,
                    runSpacing: 1,
                    alignment: WrapAlignment.spaceEvenly,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: list
                        .asMap()
                        .map((index, item) => MapEntry(
                              index,
                              item == null
                                  ? RaisedButton(
                                      elevation: 0,
                                      child: Text(''),
                                      disabledColor: Colors.grey[300],
                                    )
                                  : item is SkipItem
                                      ? RaisedButton(
                                          elevation: 0,
                                          onPressed: () => checked
                                              ? () {}
                                              : removeFromTrash(
                                                  index, item.skip),
                                          child: Text('${item.skip}',
                                              style: TextStyle(
                                                color:
                                                    getColorText(index, item),
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                              )),
                                        )
                                      : Text(item,
                                          style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                          )),
                            ))
                        .values
                        .toList(),
                  ),
                ),
              ),
            ),
            Expanded(
                flex: 0,
                child: Wrap(
                  spacing: 5,
                  runSpacing: 0,
                  alignment: WrapAlignment.spaceEvenly,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: skips
                      .map((skip) => RaisedButton(
                            elevation: 1,
                            color: Colors.white,
                            onPressed: () => addToTrash(skip),
                            child: Text(skip,
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                )),
                          ))
                      .toList(),
                ))
          ],
        ),
      ),
    );
  }

  @override
  bool get isCorrect => trash.join('_') == correctAnswers.join('_');

  @override
  void onWrong() async {
    setState(() {
      this.localActivateNext = false;
    });
    await Future.delayed(Duration(milliseconds: 1500), () {
      setState(() {
        this.localActivateNext = true;
      });
    });
  }

  Color getColorText(index, item) {
    if (!checked) return Colors.black;

    int skipIndex = list
        .where((skip) => skip is SkipItem)
        .toList()
        .indexWhere((skip) => skip == item);
    bool isCorrect = trash[skipIndex] == correctAnswers[skipIndex];
    return isCorrect ? Colors.green[400] : Colors.red[400];
  }

  void addToTrash(skip) {
    int firstEmptyIndex = list.indexWhere((item) => item == null);
    setState(() {
      list[firstEmptyIndex] = SkipItem(skip);
      skips.remove(skip);
      trash.add(skip);
    });
  }

  void removeFromTrash(int index, String skip) {
    setState(() {
      list[index] = null;
      skips.add(skip);
      trash.remove(skip);
    });
  }
}
