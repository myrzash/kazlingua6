import 'package:flutter/material.dart';
import 'package:kazlingua6/fragments/BaseGameFragment.dart';

class RadioTextFragment extends GameFragment {
  final String task;
  final List<String> answers;

  RadioTextFragment({Key key, @required this.task, @required this.answers})
      : super(key: key);

  @override
  _RadioTextFragmentState createState() => _RadioTextFragmentState();
}

class _RadioTextFragmentState extends GameFragmentState<RadioTextFragment>
    with BasicGame {
  String selected = "";

  String get correctAnswer => widget.answers[0];

  List<String> variants;

  @override
  void initState() {
    super.initState();
    variants = []
      ..addAll(widget.answers)
      ..shuffle();
  }

  @override
  String get task => 'Сәйкес жауабын тап';

  @override
  Widget body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          '${widget.task}',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w700,
          ),
        ),
        SizedBox(height: 20),
        Expanded(
          flex: 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Spacer(flex: 1),
              ListView.separated(
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    String value = variants[index];

                    return ListTile(
                      title: Text(value,
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          )),
                      onTap: checked
                          ? null
                          : () => setState(() {
                                selected = value;
                              }),
                      leading: Radio(
                        activeColor: getColorRadio(value),
                        value: value,
                        groupValue: selected,
                        onChanged: (val) => checked
                            ? null
                            : setState(() {
                                selected = val;
                              }),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) => Divider(
                        height: 1,
                        color: Colors.grey[300],
                      ),
                  itemCount: variants.length),
              Spacer(flex: 3),
            ],
          ),
        ),
      ],
    );
  }

  Color getColorRadio(word) {
    if (selected == word) {
      return checked
          ? isCorrect ? Colors.green : Colors.red
          : Color(0xFF009ed6);
    }

    return Colors.white;
  }

  @override
  bool get isCorrect => selected == correctAnswer;

  @override
  bool get activeCheck => selected != "";

  @override
  bool get activeNext => isCorrect;

  @override
  void onWrong() {
    Future.delayed(Duration(milliseconds: 1500), () {
      setState(() {
        selected = correctAnswer;
      });
    });
  }
}
