import 'package:flutter/material.dart';
import 'package:kazlingua6/fragments/BaseGameFragment.dart';
import 'package:kazlingua6/models/game/Word.dart';
import 'package:transparent_image/transparent_image.dart';

class RadioImageFragment extends GameFragment {
  final Word answer;
  final List<Word> variants;

  RadioImageFragment({Key key, @required this.variants, @required this.answer})
      : super(key: key);

  @override
  _RadioImageFragmentState createState() => _RadioImageFragmentState();
}

class _RadioImageFragmentState extends GameFragmentState<RadioImageFragment>
    with BasicGame {
  bool checked = false;
  Word selectedWord;

  @override
  Widget body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          '${widget.answer.kz}',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w700,
          ),
        ),
        SizedBox(height: 10),
        Expanded(
          flex: 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Spacer(flex: 1),
              GridView.count(
                crossAxisCount: 2,
                mainAxisSpacing: 2,
                crossAxisSpacing: 2,
                shrinkWrap: true,
                children: widget.variants
                    .map(
                      (word) => Card(
                        color: getColorCard(word),
                        elevation: selectedWord == word ? 7 : 0,
                        child: InkWell(
                          onTap: checked
                              ? null
                              : () {
                                  setState(() {
                                    selectedWord =
                                        selectedWord == word ? null : word;
                                  });
                                },
                          child: Padding(
                            padding: EdgeInsets.all(5),
                            child: FadeInImage(
                              placeholder: MemoryImage(kTransparentImage),
                              fit: BoxFit.cover,
                              image: AssetImage(
                                'assets/words/${word.image}',
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                    .toList(),
              ),
              Spacer(flex: 2),
            ],
          ),
        ),
      ],
    );
  }

  Color getColorCard(word) {
    if (selectedWord == word) {
      return checked ? isCorrect ? Colors.green : Colors.red : Colors.amber;
    }

    return Colors.white;
  }

  @override
  String get task => 'Сөзге қатысты суретті тап';

  @override
  bool get isCorrect => selectedWord.kz == widget.answer.kz;

  @override
  bool get activeCheck => selectedWord != null;

  @override
  bool get activeNext => isCorrect;

  @override
  void onWrong() {
    Future.delayed(Duration(milliseconds: 1500), () {
      setState(() {
        selectedWord = widget.answer;
      });
    });
  }
}
