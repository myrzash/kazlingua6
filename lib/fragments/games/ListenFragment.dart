import 'package:flutter/material.dart';
import 'package:kazlingua6/fragments/BaseGameFragment.dart';
import 'package:kazlingua6/helpers/SoundManager.dart';

class ListenFragment extends GameFragment {
  String sound;
  String kz;

  ListenFragment({Key key, this.sound, this.kz}) : super(key: key);

  @override
  _ListenFragmentState createState() => _ListenFragmentState();
}

class _ListenFragmentState extends GameFragmentState<ListenFragment>
    with BasicGame {
  TextEditingController controller;

  @override
  String get task => 'Тыңда және жаз';

  @override
  bool get activeCheck => controller.text.trim() != '';

  @override
  bool get activeNext => isCorrect;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
    controller.clear();
    controller.text = '';
    playAudio();
  }

  @override
  Widget body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        SizedBox(height: 30),
        IconButton(
          onPressed: playAudio,
          color: const Color(0xFF009ed6),
          iconSize: 80,
          padding: EdgeInsets.all(20),
          icon: Icon(Icons.volume_up),
        ),
        SizedBox(height: 30),
        TextField(
          autofocus: true,
          controller: controller,
          enabled: !checked,
          onEditingComplete: check,
          textInputAction: TextInputAction.done,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
          minLines: 1,
          maxLines: 3,
          decoration: InputDecoration(
            suffixIcon: getStatusIcon,
            border: OutlineInputBorder(
              borderSide: new BorderSide(color: Colors.yellow),
            ),
          ),
        ),
        SizedBox(height: 10),
      ],
    );
  }

  @override
  bool get isCorrect =>
      controller.text.trim().toLowerCase() == widget.kz.trim().toLowerCase();

  @override
  void onWrong() async {
    await Future.delayed(Duration(milliseconds: 1500), () {
      setState(() {
        controller.text = widget.kz.trim().toLowerCase();
        playAudio();
      });
    });
  }

  Icon get getStatusIcon {
    if (!checked) return null;

    return isCorrect
        ? Icon(Icons.check, color: Colors.green)
        : Icon(Icons.close, color: Colors.red);
  }

  void playAudio() {
    SoundManager.shared.playListen(localPath: widget.sound);
  }
}
