import 'package:flutter/material.dart';
import 'package:kazlingua6/views/RoundedButton.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:kazlingua6/helpers/SoundManager.dart';
import 'package:kazlingua6/models/MainModel.dart';
import 'package:kazlingua6/helpers/Prefs.dart';

class ResultGameFragment extends StatelessWidget {
  final int percent;

  ResultGameFragment({@required this.percent});

  void saveResultToStorage(key, unit) {
    Prefs.shared.saveResult(key, percent).then((_) {
      print('saveResultToStorage: $percent');
      unit.upgradeScore();
    });
  }

  @override
  Widget build(BuildContext context) {
    SoundManager.shared.playFinish();

    var unit = MainModel.of(context).currentUnit;
    if (percent > unit.score) {
      var part = MainModel.of(context).currentPart;
      saveResultToStorage('${part.name}&${unit.title}', unit);
    }

    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Spacer(
            flex: 1,
          ),
          Text(
            'Урок окончен!',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          new CircularPercentIndicator(
            radius: 120.0,
            animationDuration: 1500,
            lineWidth: 12.0,
            percent: (percent / 100),
            circularStrokeCap: CircularStrokeCap.round,
            animation: true,
            center: new Text(
              "$percent%",
              style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.bold,
              ),
            ),
            progressColor: Colors.amber[600],
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Лучший результат: ${unit.score}%',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w400,
            ),
          ),
          Spacer(
            flex: 1,
          ),
          RoundedButton(
            text: 'Повторить',
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/game',
                  arguments: ModalRoute.of(context).settings.arguments);
            },
          ),
          RoundedButton(
            text: 'Продолжить',
            onPressed: () {
              Navigator.pop(context, true);
            },
          )
        ],
      ),
    );
  }
}
