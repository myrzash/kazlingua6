import 'package:flutter/material.dart';
import 'package:kazlingua6/views/PartCard.dart';
import 'package:kazlingua6/views/MainMenu.dart';
import 'package:kazlingua6/models/DataProvider.dart';
import 'package:kazlingua6/models/Part.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:kazlingua6/models/MainModel.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<Part> parts = [];
  bool isLoading = true;

  void fetchParts() async {
    List<Part> parts = await DataProvider().fetchParts(context);
    setState(() {
      this.parts = parts;
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    isLoading = true;
    fetchParts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF009ed6),
        title: Text('ҚазLingua'),
        centerTitle: true,
      ),
      drawer: MainMenu(),
      body: isLoading
          ? Center(
              child: SpinKitFadingCube(
                color: const Color(0xFF009ed6),
                size: 50.0,
              ),
            )
          : GridView.count(
              padding: EdgeInsets.all(5),
              crossAxisCount: 2,
              childAspectRatio: (160 / 210),
              children: parts
                  .map((part) => ScopedModelDescendant<MainModel>(
                        builder: (context, _, model) => PartCard(
                          part: part,
                          onItemClick: () {
                            model.selectPart(part);
                            Navigator.pushNamed(context, '/part');
                          },
                        ),
                      ))
                  .toList(),
            ),
    );
  }
}
