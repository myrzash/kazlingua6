import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:package_info/package_info.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  String logo = 'assets/logo.png';
  String aboutApp =
      'Приложение поможет изучить казахский язык быстрее. \nБаза заданий основана на частотном словаре 6 класса.';
  String copyright =
      '© АОО «Назарбаев Интеллектуальные школы». Все права защищены. 2019 год.';
  String version = '1.0.0';

  void initAppInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      version = packageInfo.version;
    });
  }

  @override
  void initState() {
    super.initState();
    initAppInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFF009ed6),
          title: Text('О приложении'),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(40, 40, 40, 20),
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: FadeInImage(
                          placeholder: MemoryImage(kTransparentImage),
                          image: AssetImage(logo),
                        ),
                      ),
                      SizedBox(height: 10),
                      Expanded(
                        flex: 1,
                        child: Text('$version',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              letterSpacing: .5,
                            )),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Text(aboutApp,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      letterSpacing: .5,
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                    )),
              ),
              Padding(
                padding: EdgeInsets.all(15),
                child: Text(copyright,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 14,
                      letterSpacing: .5,
                    )),
              ),
            ],
          ),
        ));
  }
}
