import 'package:flutter/material.dart';
import 'package:kazlingua6/models/Part.dart';
import 'package:kazlingua6/views/UnitsPageSlider.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:kazlingua6/models/MainModel.dart';

class PartPage extends StatelessWidget {
  Part currentPart;

  Widget getLayerBG({Color bgColorHeader}) => Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(color: bgColorHeader),
          ),
          Expanded(
            flex: 1,
            child: Container(color: Colors.grey[200]),
          )
        ],
      );

  Widget getLayerCards() => Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
              flex: 4,
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.all(25),
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: Hero(
                      tag: '${currentPart.image}',
                      child: Image.asset(
                          'assets/part_icons/${currentPart.image}')),
                ),
              )),
          Expanded(
            flex: 8,
            child: UnitsPageSlider(
              colorIndicatior: currentPart.bgColor,
              units: currentPart.units,
            ),
          )
        ],
      );

  @override
  Widget build(BuildContext context) {
    currentPart = MainModel.of(context).currentPart;
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          actions: <Widget>[SizedBox(width: 40)],
          title: Text(
            currentPart.name,
            maxLines: 2,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16),
          ),
          backgroundColor: currentPart.bgColor,
        ),
        body: Stack(
          children: <Widget>[
            getLayerBG(bgColorHeader: currentPart.bgColor),
            getLayerCards(),
          ],
        ));
  }
}
