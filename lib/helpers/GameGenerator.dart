import 'package:flutter/material.dart';

import 'package:kazlingua6/models/Unit.dart';
import 'package:kazlingua6/models/game/Word.dart';
import 'package:kazlingua6/models/game/Test.dart';
import 'package:kazlingua6/models/game/Sentence.dart';
import 'package:kazlingua6/models/game/Sort.dart';
import 'package:kazlingua6/models/game/Skip.dart';
import 'package:kazlingua6/models/game/Listen.dart';

import 'package:kazlingua6/fragments/games/RadioImageFragment.dart';
import 'package:kazlingua6/fragments/games/RadioTextFragment.dart';
import 'package:kazlingua6/fragments/games/SentenceFragment.dart';
import 'package:kazlingua6/fragments/games/SortableFragment.dart';
import 'package:kazlingua6/fragments/games/SkipFragment.dart';
import 'package:kazlingua6/fragments/games/ListenFragment.dart';

class GameGenerator {
  Unit unit;
  List games = [];

  bool isGame({position}) {
    return position < games.length;
  }

  double getPercentage({position}) => 100 * position / games.length;

  GameGenerator({this.unit}) {
    games = [];

    if (unit.listens.isNotEmpty) games.addAll(unit.listens);

    if (unit.skip.isNotEmpty) games.addAll(unit.skip);

    if (unit.sorts.isNotEmpty) games.addAll(unit.sorts);

    if (unit.sentences.isNotEmpty) games.addAll(unit.sentences);

    if (unit.test.isNotEmpty) games.addAll(unit.test);

    if (unit.words.isNotEmpty) games.addAll(unit.words);

    games.shuffle();
  }

  Widget getCurrentGame({position}) {
    switch (games[position].runtimeType) {
      case Word:
        Word game = games[position];
        return new RadioImageFragment(
          key: Key('game-$position'),
          answer: game,
          variants: generateVariants(game),
        );
        break;
      case Test:
        Test game = games[position];
        return new RadioTextFragment(
          key: Key('game-$position'),
          task: game.task,
          answers: game.answers,
        );
        break;
      case Sentence:
        Sentence game = games[position];

        return new SentenceFragment(
          key: Key('game-$position'),
          task: game.ru,
          sentence: game.kz,
        );
        break;
      case Sort:
        Sort game = games[position];
        return new SortableFragment(
          key: Key('game-$position'),
          audio: game.audio,
          sentences: game.sentences,
        );
        break;
      case Skip:
        Skip game = games[position];
        return new SkipFragment(
          key: Key('game-$position'),
          list: game.list,
        );
        break;
      case Listen:
        Listen game = games[position];
        return new ListenFragment(
          key: Key('game-$position'),
          kz: game.kz,
          sound: game.sound,
        );
        break;
      default:
        return Center(child: Text('Game not found'));
        break;
    }
  }

  List<Word> generateVariants(word) {
    List<Word> otherVariants = []
      ..addAll(unit.words)
      ..removeWhere((item) => item.kz == word.kz)
      ..shuffle();
    otherVariants = otherVariants.getRange(0, 3).toList();

    return []
      ..add(word)
      ..addAll(otherVariants)
      ..shuffle();
  }

//  GameGenerator() {
//    this.unit = Unit(title: 'Test', words: [
//      Word(kz: 'ата-ана', image: 'ata-ana.jpg'),
//      Word(kz: 'нәресте', image: 'nareste.jpg'),
//      Word(kz: 'көпбалалы', image: 'kopbalaly.jpg'),
//      Word(kz: 'қария', image: 'qariya.jpg')
//    ], test: [
//      Test(
//          task: 'жаңа туылған бала',
//          answers: ["нәресте", "ауызбірлік", "қария"])
//    ], sentences: [
//      Sentence(
//          ru: "Сенің ата-анаң қайда жұмыс істейді?",
//          kz: "Менің ата-анам мектепте жұмыс істейді")
//    ], sorts: [
//      Sort(sentences: [
//        "Мұрат: Алтын, сенің бауырларың көп пе?",
//        "Алтын: Иә, мен көпбалалы отбасында өстім.",
//        "Мұрат: Ата-әжелерің бар ма?",
//        "Алтын: Иә, менің ата-әжем бар. Олар - қария адамдар."
//      ])
//    ], skip: [
//      Skip([
//        "Дүниеге келген баланы",
//        SkipItem("нәресте"),
//        "дейді. Ол дүниеге келгенде",
//        SkipItem("ата-анасы"),
//        "қатты қуанады. Ал туыстары бір-бірінен",
//        SkipItem("сүйінші"),
//        "сұрайды."
//      ])
//    ], listens: [
//      Listen(kz: 'нәресте', sound: 'sounds/listens/1.mp3'),
//      Listen(kz: 'қария', sound: 'sounds/listens/2.mp3')
//    ]);
//    games = []
//      ..addAll(unit.listens)
//      ..addAll(unit.skip)
//      ..addAll(unit.sorts)
//      ..addAll(unit.sentences)
//      ..addAll(unit.test)
//      ..addAll(unit.words)
//      ..shuffle();
//  }

}
