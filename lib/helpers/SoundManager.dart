import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';

class SoundManager {
  AudioPlayer audioPlayer;
  AudioCache audioCache;

  SoundManager.init() {
    audioPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: audioPlayer);
  }

  static final SoundManager shared = SoundManager.init();

  void playCorrect() async {
    await audioCache.play('sfx/true_sound.mp3');
  }

  void playError() async {
    await audioCache.play('sfx/error_sound.mp3');
  }

  void playFinish() async {
    await audioCache.play('sfx/finish_sound.mp3', volume: 0.2);
  }

  void stopAudio() async {
    await audioPlayer.stop();
  }

  void playListen({String localPath}) async {
    await audioCache.play(localPath.trim());
  }
}
